Role Name
=========

windows_exporter: Configure windows_exporter service collectors and process whitelist.

Requirements
------------

Gitlab runner with ansible and docker engine

Role Variables
--------------

**ansible_port:** Specifies the WinRM port. Default: 5986  
**ansible_winrm_kinit_mode:** Specifies the kerberos initialization method. Manual requires one time "kinit" on the command line prior to execution but is more compatible with Redhat Identity Manager. Default: manual  
**ansible_connection:** Specifies the connection method. Default: winrm  
**ansible_winrm_transport:** Specifies the WinRM transport and authentication method. Default: kerberos  
**ansible_winrm_server_cert_validation:** Specifies weather to ignore certificate errors (for self-signed certs). Default: ignore  
**ansible_winrm_scheme:** Specifies the transport method. Default: https  

Dependencies
------------

The runner and target hosts must be configured to allow secure WinRM communication: See the Ansible [Windows Guide](https://docs.ansible.com/ansible/latest/user_guide/windows.html).

Example Playbook
----------------

---
```
- hosts: pilot
  roles:
    - windows_exporter
```

License
-------

(c) 1st Technologies, Inc

Author Information
------------------

1st Technologies Inc.
http://www.1st-technologies.com
